# Telegram Screening Bot

This bot solves task of technical pre-screening

## Setup

```
pip install -r requirements.txt
python app.py $TOKEN $PATH_TO_QUESTIONS_YAML
```

## How to use

>>>
Use system env HTTPS_PROXY so bot will connect to telegram servers with it.
>>>
```
Usage: app.py [OPTIONS] BOT_TOKEN QUESTIONS_PATH

Options:
  -d, --database_path TEXT
  -r, --recruiter_command TEXT
  --help                        Show this message and exit.
```

### For recruiters


1. Send `/recruit`
1. Enter candidate's e-mail
    - To enable e-mail
    - To check results of screening   

>>>
Send `/cancel` if it is your first conversation with bot

By default bot treats everybody as a candidate
>>>

### For candidates

1. Enter e-mail
1. Screening process begins

>>>
Complete screening to see your results

You are not allowed to use this email anymore

Even from different telegram account
>>>

## Todo

- [ ] Add docstrings
- [ ] Create setup.py
- [x] Add some extra lint
- [x] Keyboard many variants formatting
- [x] Random order for answers
- [x] Inform candidate of total questions quantity