import click as click

from src.bot import BotRunner
from src.processor import ScreeningProcessor


@click.command()
@click.argument("bot_token", type=str)
@click.argument("questions_path")
@click.option("-d", "--database_path", default="./db.json")
@click.option("-r", "--recruiter_command", default="recruit")
def cli(bot_token, questions_path, database_path, recruiter_command):
    screening_processor = ScreeningProcessor(
        database_path=database_path, questions_path=questions_path
    )
    bot_runner = BotRunner(screening_processor, recruiter_command)
    bot_runner.run(bot_token)


if __name__ == "__main__":
    cli()
