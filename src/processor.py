import logging
import time
import uuid
from dataclasses import dataclass
from datetime import datetime, timedelta
from functools import wraps

import yaml
from tinydb import TinyDB, Query
from tinydb.operations import set as db_set


@dataclass
class Results:
    """Designed for results representation"""
    email: str
    score: int
    start: datetime
    end: datetime
    duration: timedelta
    details: str

    def __str__(self):
        return (
            f"*E-mail*: `{self.email}`\n"
            f"*Score*: `{self.score}%`\n"
            f"\n"
            f"*Started at*: `{self.start}`\n"
            f"*Ended at*: `{self.end}`\n"
            f"*Duration*: `{self.duration}`\n"
            f"\n"
            f"*Details*:\n{self.details}"
        )


def get_logger(name):
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
    )

    return logging.getLogger(name)


LOGGER = get_logger(__name__)


def generate_screening_token(user_id):
    return f"{user_id}-{uuid.uuid1()}"


def safe_list_get(items: list, i: int):
    try:
        return items[i]
    except IndexError:
        return None


def method_info(func):
    @wraps(func)
    def wrapper(cls, *args, **kwargs):
        ret = func(cls, *args, **kwargs)
        LOGGER.info("%s", dict(func=func.__name__, args=args, kwargs=kwargs, ret=ret))
        return ret

    return wrapper


class ScreeningProcessor:
    @method_info
    def __init__(self, database_path, questions_path):
        with open(questions_path) as stream:
            self.questions = yaml.safe_load(stream)
        database = TinyDB(database_path)
        self.candidates = database.table("candidates")
        self.accepted_emails = database.table("accepted_emails")
        self.total_questions = len(self.questions)

    @method_info
    def get_email(self, user_id):
        return self.candidates.search(Query().user_id == user_id)[0]["email"]

    @method_info
    def create_candidate(self, user_id, email):
        self.candidates.insert(
            dict(user_id=user_id, email=email, ts=int(time.time()), answers=[])
        )

    @method_info
    def add_email_to_accept_list(self, email):
        enabled_at = datetime.today().strftime("%d.%m.%Y %H:%M:%S")
        self.accepted_emails.insert(
            dict(email=email, enabled_at=enabled_at, in_use=False, disabled_at=None)
        )

    @method_info
    def mark_candidate_email_disabled(self, user_id):
        email = self.candidates.search(Query().user_id == user_id)[0]["email"]
        now = datetime.today()
        disabled_at = now.strftime("%d.%m.%Y %H:%M:%S")
        self.accepted_emails.update(
            db_set("disabled_at", disabled_at), Query().email == email
        )

    @method_info
    def write_answer_to_db(self, user_id, question, answer):
        user_data = self.candidates.search(Query().user_id == user_id)
        answers = user_data[0]["answers"]
        question_answer = dict(question=question, answer=answer, ts=int(time.time()))
        answers.append(question_answer)
        self.candidates.write_back(user_data)

    @method_info
    def candidate_exists(self, user_id) -> bool:
        """Checks if candidate present in db"""
        res = self.candidates.search(Query().user_id == user_id)
        if len(res) > 1:
            raise Exception(
                f'Two or more entries found for user_id: "{user_id}". This should never happen!'
            )
        return len(res) == 1

    @method_info
    def email_is_accepted(self, email) -> bool:
        """Checks e-mail address is in accepted list."""
        res = self.accepted_emails.search(Query().email == email)
        if len(res) > 1:
            raise Exception(
                f'Two or more entries found for e-mail: "{email}". This should never happen!'
            )
        return len(res) == 1

    @method_info
    def mark_email_in_use(self, user_id):
        email = self.get_email(user_id)
        self.accepted_emails.update(db_set("in_use", True), Query().email == email)

    @method_info
    def email_is_disabled_or_used(self, email):
        """Checks e-mail address is disabled or in use"""

        res = self.accepted_emails.search(Query().email == email)
        if len(res) > 1:
            raise Exception(
                f'Two or more entries found for e-mail: "{email}". This should never happen!'
            )
        return len(res) == 1 and (res[0]["disabled_at"] or res[0]["in_use"])

    @method_info
    def get_answers(self, user_id=None, email=None):
        if user_id:
            email = self.candidates.search(Query().user_id == user_id)[0]["email"]
        res = self.candidates.search(Query().email == email)
        return res[0]["answers"] if res else []

    @method_info
    def get_starting_ts(self, user_id=None, email=None):
        if user_id:
            email = self.candidates.search(Query().user_id == user_id)[0]["email"]
        res = self.candidates.search(Query().email == email)
        return res[0]["ts"] if res else []

    @method_info
    def get_email_info(self, user_id=None, email=None):
        if user_id:
            email = self.candidates.search(Query().user_id == user_id)[0]["email"]
        return self.accepted_emails.search(Query().email == email)[0]

    @method_info
    def get_last_unanswered_question(self, user_id):
        return safe_list_get(self.questions, len(self.get_answers(user_id)))

    @method_info
    def get_results(self, user_id=None, email=None) -> Results or str:

        if user_id:
            email = self.get_email(user_id)
        answers = self.get_answers(user_id=user_id, email=email)
        if answers:
            end_ts = self.get_answers(user_id=user_id, email=email)[-1]["ts"]
            end = datetime.fromtimestamp(end_ts)
            start_ts = self.get_starting_ts(user_id=user_id, email=email)
            start = datetime.fromtimestamp(start_ts)
            correct_answers = [x["question"]["correct"] for x in answers]
            candidate_answers = [x["answer"] for x in answers]
            duration = end - start
            candidates_correct_incorrect_answers = [
                x == y for x, y in list(zip(correct_answers, candidate_answers))
            ]
            details = [
                f"{x} - {y} {'✅' if z else '🛑'}"
                for (x, y, z) in zip(
                    [x["question"]["question"] for x in answers],
                    candidate_answers,
                    candidates_correct_incorrect_answers,
                )
            ]
            details = "\n".join(details)
            correct_quantity = candidates_correct_incorrect_answers.count(True)
            score = int(correct_quantity / len(correct_answers) * 100)
            return Results(
                email=email,
                score=score,
                start=start,
                end=end,
                duration=duration,
                details=details,
            )
        return f"No results yet for `{email}`."
