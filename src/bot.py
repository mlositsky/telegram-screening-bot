import itertools
from functools import wraps
from random import shuffle

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import (
    Updater,
    ConversationHandler,
    CommandHandler,
    MessageHandler,
    Filters,
)

from src.processor import ScreeningProcessor, get_logger

logger = get_logger(__name__)

(
    CANDIDATE_ENTERED_EMAIL,
    SCREENING_OVER,
    ASK_QUESTION,
    HANDLE_ANSWER,
    RECRUITER_ENTERED_CANDIDATES_EMAIL,
) = range(5)


def handler_info(func):
    @wraps(func)
    def wrapper(cls, update, context):
        if update:
            user_id = update.effective_user.id
            full_name = update.message.from_user.full_name
            username = update.message.from_user.username
            logger.info(
                f'username: "{username}" full_name: "{full_name}" id: "{user_id}" called: "{func.__name__}"'
            )
        return func(cls, update, context)

    return wrapper


def prepare_keyboard(arr):
    new_arr = arr.copy()
    shuffle(new_arr)

    def clean(iterable):
        return list(filter(lambda x: x is not None, iterable))

    def pairwise(iterable, length=2):
        """Example:
            transforms list [a,b,c,d,e,f,g] to
            [[a,b],
             [c,d],
             [e,f],
             [g]]

        Args:
            iterable(list):
            length(int):

        Returns: list of lists see example

        """
        a = iter(iterable)
        return map(clean, itertools.zip_longest(*[a] * length))

    return pairwise(new_arr, 2)


class BotRunner:
    def __init__(
            self, screening_processor: ScreeningProcessor, command_for_recruiters: str
    ):
        self.screening_processor = screening_processor
        self.command_for_recruiters = command_for_recruiters

    @handler_info
    def start(self, update, _):
        update.message.reply_text(f"Hello {update.message.from_user.full_name},")
        update.message.reply_markdown(
            "Please enter your e-mail address\n" "to initialize screening session.",
            reply_markup=ReplyKeyboardRemove(),
        )
        return CANDIDATE_ENTERED_EMAIL

    @handler_info
    def start_recruit(self, update, _):
        update.message.reply_markdown(
            "Please enter candidate's email address.\n"
            "- to register\n"
            "- to get results",
            reply_markup=ReplyKeyboardRemove(),
        )
        return RECRUITER_ENTERED_CANDIDATES_EMAIL

    @handler_info
    def handle_candidate_email(self, update, _):
        email = update.message.text.lower()
        results = self.screening_processor.get_results(email=email)
        if not self.screening_processor.email_is_accepted(email):
            update.message.reply_markdown(
                f"E-mail: `{email}` successfully added to accept list!"
            )
            self.screening_processor.add_email_to_accept_list(email)
        else:
            update.message.reply_markdown(
                str(results), reply_markup=ReplyKeyboardRemove()
            )
        return ConversationHandler.END

    @handler_info
    def check_email(self, update, _):
        email = update.message.text.lower()
        user_id = update.effective_user.id
        if self.screening_processor.email_is_disabled_or_used(email):
            logger.info(f'User: "{user_id}" entered used e-mail: "{email}"')
            update.message.reply_markdown(
                f"E-mail: `{email}` has been already used!\nPlease try again!"
            )
            return CANDIDATE_ENTERED_EMAIL
        elif self.screening_processor.email_is_accepted(email):
            logger.info(f"Candidate {user_id} entered e-mail {email}.")
            if not self.screening_processor.candidate_exists(user_id):
                self.screening_processor.create_candidate(user_id, email)
                self.screening_processor.mark_email_in_use(user_id)
            reply_keyboard = [["I'm ready!"]]
            update.message.reply_markdown(
                f"Prepare to answer `{self.screening_processor.total_questions}` questions\n"
                f"{update.message.from_user.full_name} are you ready?",
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, resize_keyboard=True),
            )
            return ASK_QUESTION
        else:
            logger.info(
                f'User: "{user_id}" entered missing from the access list e-mail: "{email}"'
            )
            update.message.reply_markdown(
                f"E-mail: `{email}` is missing from access list!\nPlease try again!"
            )
            return CANDIDATE_ENTERED_EMAIL

    @handler_info
    def end_screening(self, update, _):
        user_id = update.effective_user.id
        self.screening_processor.mark_candidate_email_disabled(user_id)
        update.message.reply_text(f"Good bye {update.message.from_user.full_name}!")
        results = self.screening_processor.get_results(user_id)
        update.message.reply_markdown(str(results), reply_markup=ReplyKeyboardRemove())
        return ConversationHandler.END

    @handler_info
    def cancel(self, update, _):
        update.message.reply_text(
            "Cancel triggered.", reply_markup=ReplyKeyboardRemove()
        )
        return ConversationHandler.END

    @handler_info
    def error(self, update, context):
        """Log Errors caused by Updates."""
        logger.error(f'Update "{update}" caused error "{context.error}"')

    @handler_info
    def ask_question(self, update, _):
        user_id = update.effective_user.id
        question = self.screening_processor.get_last_unanswered_question(user_id)
        if not question:
            reply_keyboard = [["ok"]]
            update.message.reply_text(
                f'Dear {update.message.from_user.full_name}, technical screening is over, press "ok" to continue.',
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, resize_keyboard=True, ),
            )
            return SCREENING_OVER
        else:
            reply_keyboard = prepare_keyboard(question["variants"])
            update.message.reply_markdown(
                question["question"],
                reply_markup=ReplyKeyboardMarkup(reply_keyboard, resize_keyboard=True),
            )

            return HANDLE_ANSWER

    @handler_info
    def handle_answer(self, update, _):
        user_id = update.effective_user.id
        question = self.screening_processor.get_last_unanswered_question(user_id)
        answer = update.message.text
        self.screening_processor.write_answer_to_db(user_id, question, answer)
        reply_keyboard = [["ok"]]
        update.message.reply_text(
            f"next question?",
            reply_markup=ReplyKeyboardMarkup(reply_keyboard, resize_keyboard=True),
        )
        return ASK_QUESTION

    def run(self, bot_token):
        updater = Updater(bot_token, use_context=True)
        conversation_handler = ConversationHandler(
            entry_points=[
                CommandHandler("start", self.start),
                CommandHandler(self.command_for_recruiters, self.start_recruit),
            ],
            states={
                RECRUITER_ENTERED_CANDIDATES_EMAIL: [
                    MessageHandler(Filters.text, self.handle_candidate_email)
                ],
                CANDIDATE_ENTERED_EMAIL: [
                    MessageHandler(Filters.text, self.check_email)
                ],
                SCREENING_OVER: [
                    MessageHandler(Filters.regex("^ok$"), self.end_screening)
                ],
                ASK_QUESTION: [
                    MessageHandler(
                        Filters.regex("^(yes|ok|I'm ready!)$"), self.ask_question
                    )
                ],
                HANDLE_ANSWER: [MessageHandler(Filters.text, self.handle_answer)],
            },
            fallbacks=[CommandHandler("cancel", self.cancel), ],
        )
        updater.dispatcher.add_handler(conversation_handler)
        updater.dispatcher.add_error_handler(self.error)
        updater.start_polling()
        updater.idle()
